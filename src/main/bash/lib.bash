#!/bin/bash

check_for_command () {
  command -v "$1" >/dev/null 2>&1 || {
    echo >&2 "Must have $1 installed, aborting."; exit 1;
  }
}

