#!/bin/bash

. src/main/bash/lib.bash

check_for_command jq
check_for_command wget

url="https://api.github.com/search/repositories?q="
pages="$1"
query="$2"
outfile="$3"

[[ "$#" -ne 3 ]] && echo "Requires 3 arguments: #pages, query and output file" && exit 1
case $pages in *[!0-9]*) echo "First argument must be an integer" && exit 1 ;; esac
[[ -f "$outfile" ]] && rm "$outfile"
[[ -e "$outfile" ]] && echo "$outfile exists and is not a regular file, aborting" && exit 1

echo "Crawling GitHub for $((pages*100)) projects, writing urls to $outfile"

for (( i=0; i<=$pages; i++ )); do
  echo -n "$((i*100))..."
  wget -q "$url""$query"'&sort=stars&order=desc&per_page=100&page='"$i" -O - \
    | jq '.items[].git_url' \
    | sed -e 's/^"//g;s/"$//g' \
    >> "$outfile"
done
echo

