#!/bin/bash

n="$1"
cwd="$2"
filename="$3"

cd "$cwd"
head -n "$n" "$filename" > "$filename".dev
tail -n "$n" "$filename" >> "$filename".dev
tail -n +"$((n+1))" "$filename" | head -n "25000000" > "$filename".train

