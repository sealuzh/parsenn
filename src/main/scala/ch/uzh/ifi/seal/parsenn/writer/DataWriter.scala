package ch.uzh.ifi.seal.parsenn.writer

import scala.io.Source
import scala.collection.mutable.{Map => MMap}
import scala.collection.mutable.ListBuffer
import com.typesafe.scalalogging.Logger
import com.typesafe.scalalogging.LazyLogging
import java.io.FileWriter
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.Path

import ch.uzh.ifi.seal.parsenn.parser.NodeData

object SourceFormatter {
  def label: String = "source"
  def logger = Logger(label)
  def mkWords(line: String): Seq[String] = {
    line.map(_.toString)
  }
  def mkInts(line: String, vocab: Vocab): Seq[Int] = {
    mkWords(line).map { w => vocab.addWord(w) }
  }
}

/** Creates vocab and writes input/output files
  *
  * The write method accepts an input and corresponding output sequence
  * representing a single datum of the dataset. The class takes care of writing
  * the datum both in text as well as numbers of a vocabulary that is built
  * on the fly. The close method needs to be called to persist the vocabulary
  * and close all the files written to.
  */
class DataWriter(outDir: Path, tokenFormatters: List[TokenFormatter] = TokenFormatter.none, astNodeFormatters: List[AstNodeFormatter] = AstNodeFormatter.none) extends LazyLogging {
  // create output directory if necessary
  outDir.toFile.mkdirs
  outDir.resolve("tok").toFile.mkdirs
  outDir.resolve("ast").toFile.mkdirs

  val writers = MMap[String, FileWriter]()
  val vocabs = MMap[String, Vocab]()

  private def escape(word: String): String = {
    return word.replaceAll("\\s", "\uff00")
  }

  private def getWriter(fileName: String): FileWriter = {
    writers.getOrElseUpdate(fileName, new FileWriter(outDir.resolve(Paths.get(fileName)).toFile))
  }
  private def getVocab(fileName: String): Vocab = {
    vocabs.getOrElseUpdate(fileName, new Vocab(outDir, fileName))
  }

  def writeTokens(line: String, tokens: List[String]) {
    val results = tokenFormatters.map { f =>
      val words = f.mkWords(line, tokens)
      (f.label, words, f.mkInts(line, tokens, words, getVocab(f.label)))
    }
    if (!results.exists(_._2.isEmpty)) {
      write("tok/source", line)
      write("tok/source-chars", SourceFormatter.mkWords(line).map(escape(_)).mkString(" "))
      write("tok/source-chars.ints.unsorted", SourceFormatter.mkInts(line, getVocab("tok/source-chars")).mkString(" "))
      results.foreach { case (label, words, ints) => 
        write(label, words.map(escape(_)).mkString(" "))
        write(label + ".ints.unsorted", ints.mkString(" "))
      }
    }
  }

  def writeAstNodes(line: String, nodes: List[NodeData]) {
    val results = astNodeFormatters.map { f =>
      val words = f.mkWords(line, nodes)
      (f.label, words, f.mkInts(line, nodes, words, getVocab(f.label)))
    }
    if (!results.exists(_._2.isEmpty)) {
      val astTokens = nodes.map(_.text)
      val inputVocab = getVocab("ast/tokens")
      val astTokenInts = astTokens.map{inputVocab.addWord(_)}
      write("ast/source", line)
      write("ast/tokens", astTokens.map{ x => escape(x.toString)}.mkString(" "))
      write("ast/tokens.ints.unsorted", astTokenInts.mkString(" "))
      results.foreach { case (label, words, ints) => 
        write(label, words.map(escape(_)).mkString(" "))
        write(label + ".ints.unsorted", ints.mkString(" "))
      }
    }
  }

  def write(fileName: String, data: String) {
    // write input and output to files "as is"
    val writer = getWriter(fileName)
    writer.write(data)
    writer.write('\n')
  }

  /** splits a file into training and dev sets (10% of data by default) **/
  private def splitData(path: Path, n: Int = -1) {
    val f = Source.fromFile(path.toFile)
    val dataLines = Source.fromFile(path.toFile).getLines.size
    val writerTrain = new FileWriter(path.getParent().resolve(path.getFileName() + ".train").toFile)
    val writerDev = new FileWriter(path.getParent().resolve(path.getFileName() + ".dev").toFile)
    val devLines = if (n < 0) (math.min(dataLines * 0.1, 1000000)).toInt else n
    val lineLengths = ListBuffer[Int]()
    logger.info(s" > splitting data (${path.getFileName}, ${dataLines} lines) into training and dev (${devLines} lines)...")
    f.getLines.zipWithIndex.foreach { case (l, i) =>
      lineLengths += l.size
      if (i <= devLines) { writerDev.write(l); writerDev.write('\n') }
      else { writerTrain.write(l); writerTrain.write('\n') }
    }
    writerTrain.close
    writerDev.close
  }

  /** Close writers and create final data set */
  def close() {
    writers.foreach { case (_, writer) => writer.close() }
    vocabs.foreach { case (fileName, vocab) =>
      logger.info(s"Finalizing ${fileName}")
      writers += ((fileName + ".ints", vocab.postprocess))
    }
    writers.foreach { case (fileName, _) =>
      // split embedded data
      //if (fileName.endsWith(".ints")) {
      //  splitData(outDir.resolve(Paths.get(fileName)))
      //}
      // delete unsorted data
      if (fileName.endsWith(".unsorted")) {
        Files.delete(outDir.resolve(Paths.get(fileName)))
      }
    }
  }
}


