package ch.uzh.ifi.seal.parsenn.parser

import scala.collection.mutable.{Map => MMap}
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Stack
import org.antlr.v4.runtime.tree.RuleNode
import org.antlr.v4.runtime.tree.TerminalNode
import org.antlr.v4.runtime.RuleContext
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor
import org.antlr.v4.runtime.Lexer
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.{Parser => RuntimeParser}
import org.antlr.v4.runtime.atn.PredictionMode
import java.lang.reflect.ParameterizedType
import sun.reflect.generics.reflectiveObjects.TypeVariableImpl
import java.lang.reflect.Method
import java.lang.reflect.Type
import java.util.{List => JavaList}
import scala.reflect.ClassTag
import com.typesafe.scalalogging.LazyLogging
import org.antlr.v4.runtime.ANTLRInputStream
import org.antlr.v4.runtime.CommonTokenStream
import scala.collection.JavaConverters._
import java.nio.CharBuffer
import ch.uzh.ifi.seal.parsenn.writer._

/** A generic parser/visitor combo for use with ANTLR generated parsers
  *
  * This class is used to add support for additional languages through ANTLR
  * generated lexers and parsers. It provides a generic visitor that will
  * traverse arbitrary parse trees and generate vertices and edges in the graph
  * based on the provided Domain such that only those vertices which have a
  * mapping in the Domain will be parsed into the graph.
  *
  * @tparam P the ANTLR parser class wrapped by this AntlrParser
  * @param domain the Domain (parse tree) used with this parser
  * @param filter if true, parse only those vertices having a mapping in domain
  */
abstract class AntlrParser[P <: RuntimeParser : ClassTag](domain: Domain, filter: Boolean = true) extends Parser with LazyLogging {
  def lex(input: ANTLRInputStream): Lexer
  def parse(tokens: CommonTokenStream): P
  def enter(parser: P): ParserRuleContext

  // To accelerate the parsing, all reflection is done once beforehand. There
  // are three caches, one for class -> class.getSimpleName lookups, one for
  // class -> class.getMethods lookups and one for method name ->
  // method.getGenericReturnType lookups
  val typeCache: (Map[Class[_],String],
                  Map[Class[_],Array[Method]],
                  Map[Method,Type]) = {
    // retrieve the parser class
    val parserClass = implicitly[ClassTag[P]].runtimeClass
    // retrieve all the inner (*Context) classes defined by the parser
    val contextClasses = parserClass.getDeclaredClasses()
    // for each of those classes...
    contextClasses.foldLeft((Map[Class[_],String](),
                             Map[Class[_],Array[Method]](),
                             Map[Method,Type]())) { case (acc, ctx) =>
      // store a mapping from the class to its string name
      val contextName = ctx.getSimpleName match {
        case s if s.endsWith("Context") => s.dropRight(7)
        case s => s
      }
      val methods = ctx.getMethods.filter(_.getName != "getTokens")
      (acc._1 + (ctx -> contextName),
      acc._2 + (ctx -> methods),
      // and store the return types of all methods defined by the class
      methods.foldLeft(acc._3) { case (acc, m) =>
        acc + (m -> m.getGenericReturnType)
      })
    }
  }

  /** Parse the given character buffers
   *
   *  @param files the list of (file name, character buffer) tuples
   *  @param writer the DataWriter to use for writing parsing results
   *  @param method the output method ("default" by default)
   */
  override def parse(files: List[(String,CharBuffer)], writer: DataWriter) {
     tokenize(files, writer)
     parseASTs(files, writer)
  }

  def tokenize(files: List[(String,CharBuffer)], writer: DataWriter) {
    val inputs = files.map { case (path, code) =>
      (path, new ANTLRInputStream(code.toString.toCharArray(), code.length))
    }
    inputs.foreach { case (p, input) => tokenizeFile(p, input) }

    def tokenizeFile(p: String, input: ANTLRInputStream): Unit = {
      // tokenization only:
      val lines = input.toString.split("\\r?\\n");
      if (lines.isEmpty) return
      val lexer = lex(input)
      lexer.removeErrorListeners()
      val tokens = new CommonTokenStream(lexer)
      tokens.fill()
      //println(tokens.getText)
      var currentLine = 1
      var lineTokens = ListBuffer[String]()
      for (t <- tokens.getTokens.asScala) {
        val l = t.getLine
        if (l > currentLine) {
          val line = lines(currentLine-1).trim
          writer.writeTokens(line, lineTokens.toList)
          currentLine = l
          lineTokens.clear()
        }
        lineTokens += t.getText
      }
    }
  }

  def parseASTs(files: List[(String,CharBuffer)], writer: DataWriter) {
    val inputs = files.map { case (path, code) =>
      (path, new ANTLRInputStream(code.toString.toCharArray(), code.length))
    }
    inputs.foreach { case (p, input) => parseFile(p, input) }

    /** TODO: parseFile is not implemented properly, not used at the moment */
    def parseFile(p: String, input: ANTLRInputStream) {
      val lines = input.toString.split("\\r?\\n");
      if (lines.isEmpty) return
      val path = "/" + p // must match the path used by Git service
      val lexer = lex(input)
      lexer.removeErrorListeners()
      // regular parsing:
      val tokens = new CommonTokenStream(lexer)
      val parser = parse(tokens)
      parser.removeErrorListeners()
      // two stage parsing (for speed) as explained in various locations
      // https://github.com/antlr/antlr4/issues/374#issuecomment-30952357
      // http://www.antlr.org/api/Java-master/org/antlr/v4/runtime/atn/ParserATNSimulator.html
      parser.getInterpreter().setPredictionMode(PredictionMode.SLL)
      val tree = try { enter(parser) }
      catch {
        case e: Exception =>
          tokens.reset()
          parser.reset()
          parser.getInterpreter().setPredictionMode(PredictionMode.LL)
          enter(parser)
      }
      val visitor = new UniversalVisitor(path, domain, tokens, filter, typeCache, lines)
      val labels: List[NodeData] = visitor.visit(tree)
      var currentLine = 1
      var lineNodes = ListBuffer[NodeData]()
      for (t <- labels) {
        val l = t.lineNumber
        if (l > currentLine) {
          val line = lines(currentLine-1).trim
          writer.writeAstNodes(line, lineNodes.toList)
          currentLine = l
          lineNodes.clear()
        }
        lineNodes += t
      }
    }
  }


}

/** A generic visitor for arbitrary ANTLR generated parse trees
  *
  * Normally, a user would implement visitor methods for each of the context
  * types defined by the parser. Since we need to work with arbitrary tree, only
  * one method, visitChildren, is implemented and called for all context types.
  * The visitor automatically extracts literals using reflection.
  *
  * @param fileName the file name of the file being parsed
  * @param graph the graph to be populated
  * @param domain the domain of the newly created vertices
  * @param filter if true, parse only those vertices having a mapping in domain
  * @param typeCache the pre-generated cache used to avoid repeated reflection
  */
class UniversalVisitor(fileName: String, domain: Domain,
                       tokens: CommonTokenStream, filter: Boolean = true,
                       typeCache: (Map[Class[_],String],
                                   Map[Class[_],Array[Method]],
                                   Map[Method,Type]), val lines: Array[String])
extends AbstractParseTreeVisitor[List[NodeData]] {

  var parentUri = fileName
  val chainAST = Stack[String](parentUri)
  var chainGraph = Stack[String](parentUri)
  val forksAST = MMap[String, Int]()
  val forksGraph = MMap[String, Int]()
  val lineTokens = MMap[Int, ListBuffer[String]]()
  val lineNodes = MMap[Int, ListBuffer[String]]()

  // graphDepth represents the traversal depth of the *filtered* graph. The
  // depth of the AST is retrieved from ctx.getRuleContext.depth
  var depthGraph = 0

  def printBuffers() = {
    for ( n <- 1 to lineTokens.size) {
      println(n)
      print("line:   "); println(lines(n-1).trim)
      print("tokens: "); println(lineTokens.getOrElse(n, List()).mkString(" "))
      print("nodes:  "); println(lineNodes.getOrElse(n, List()).mkString(" "))
    }
  }

  override def visitTerminal(ctx: TerminalNode): List[NodeData] = {
    val tokenInterval = ctx.getSourceInterval
    val line = ctx.getPayload match {
      case t: Token => t.getLine
      case _ => -1
    }
    if (tokenInterval.a == tokenInterval.b) {
      val parent = ctx.getParent.getPayload
      parent match {
        case n: RuleContext =>
         //println(s"${tokenInterval}, ${n.getClass.getSimpleName}, ${n.depth}: ${ctx.getText}")
         List(NodeData(ctx.getText, line, n.getClass.getSimpleName.dropRight(7), n.depth, ctx))
        case _ => defaultResult
      }
    }
    else {
      defaultResult
    }
  }

  override def defaultResult() = List[NodeData]()

  override def aggregateResult(aggregate: List[NodeData],
                               nextResult: List[NodeData]): List[NodeData] = {
    aggregate ++ nextResult
  }

  override def visitChildren(ctx: RuleNode): List[NodeData] = {
    val contextClass = ctx.getClass
    // get local vertex type label
    val name = typeCache._1.get(contextClass).get

    val d = ctx.getRuleContext.depth
    while (chainAST.size > d) {
      chainAST.pop
    }

    //var lineFirstToken: Option[Token] = None
    //var lineLastToken: Option[Token] = None

    val parentUriAST = chainAST.lastOption.map {
      l => chainAST.init.foldRight(new StringBuilder(l)) {
        (e, acc) => acc.append("/").append(e)
      }.result
    }.getOrElse("")

    val uriNonIndexedAST = parentUriAST + "/" + name
    val index = forksAST.getOrElse(uriNonIndexedAST, 0)
    forksAST += (uriNonIndexedAST -> (index + 1))
    chainAST.push(name + index)

    if (domain.labels.contains(name) || !filter) {
      depthGraph += 1

      while (chainGraph.size > depthGraph) {
        chainGraph.pop
      }

      val parentUriGraph = chainGraph.lastOption.map {
        l => chainGraph.init.foldRight(new StringBuilder(l)) {
          (e, acc) => acc.append("/").append(e)
        }.result
      }.getOrElse("")

      val uriNonIndexedGraph = parentUriGraph + "/" + name
      val index = forksGraph.getOrElse(uriNonIndexedGraph, 0)
      forksGraph += (uriNonIndexedGraph -> (index + 1))
      chainGraph.push(name + index)

      val uri = uriNonIndexedGraph + index

      // Literals are terminal nodes
      typeCache._2.get(contextClass).get.foreach { m =>
        val returnType = typeCache._3.get(m)
        returnType.get match {
          case t: TypeVariableImpl[_] =>
          // find methods returning List[TerminalNode]
          case t: ParameterizedType =>
            val rawType = t.getRawType
            val typeArguments = t.getActualTypeArguments
            val isList = rawType match {
              case r: Class[_] => classOf[JavaList[_]].isAssignableFrom(r)
              case _ => false
            }
            if (isList) {
              val containsTerminalNodes = typeArguments.headOption match {
                case Some(tpe: TypeVariableImpl[_]) => false
                case Some(tpe) if classOf[TerminalNode].isAssignableFrom(tpe.asInstanceOf[Class[_]]) => true
                case _ => false
              }
              if (containsTerminalNodes) {
                val children = Option(m.invoke(ctx).asInstanceOf[JavaList[TerminalNode]])
                children match {
                  case Some(children) =>
                    //children.asScala.map { child => print(child.toString) }
                    //val literals = children.asScala.zipWithIndex
                    //    .foldLeft(state[Literal].map) { case (acc, (child, i)) =>
                    //  acc + (m.getName + i.toString -> child.toString)
                    //}
                  case None =>
                }
              }
            }
          // find methods returning TerminalNode
          case t if classOf[TerminalNode].isAssignableFrom(t.asInstanceOf[Class[_]]) =>
            if (m.getGenericParameterTypes().size == 0) {
              val result = m.invoke(ctx)
              if (result != null) {
                //print(result)
                //val literals = state[Literal].map
              }
            }
          case t =>
        }
      }

    }
    var hasChildren = ctx.getRuleContext.getChildCount > 0
    //if (ctx.getRuleContext.getChildCount == 1) {
    //  ctx.getRuleContext.getChild(0) match {
    //    case t: TerminalNode => hasChildren = false
    //    case _ =>
    //  }
    //}
    //val nodeToken = s"${name}[${ctx.getRuleContext.getChildCount}]"
    val sourceInterval = ctx.getRuleContext.getSourceInterval
    val firstToken = tokens.get(sourceInterval.a)
    val line = firstToken.getLine()
    if (domain.labels.contains(name) || !filter) {
      val lts = lineNodes.getOrElse(line, ListBuffer[String]())
      lts += name
      lineNodes += ((line, lts))
    }
    //val indent = " " * depthGraph
    //val nodeToken = s"${indent}${name}[${line}]"
    //if (hasChildren) { print(s"\n${nodeToken}( ") }
    //else {             print(s"\n${nodeToken} ") }
    val res = super.visitChildren(ctx)
    //if (hasChildren) { print(s"\n${indent})[${line}] ") }
    if (domain.labels.contains(name) || !filter) {
      depthGraph -= 1
    }
    res
  }
}

