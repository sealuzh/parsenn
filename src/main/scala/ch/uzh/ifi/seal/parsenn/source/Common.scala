package ch.uzh.ifi.seal.parsenn.source

import scala.collection.immutable.SortedMap
import java.nio.CharBuffer

import ch.uzh.ifi.seal.parsenn.parser.Parser
import ch.uzh.ifi.seal.parsenn.writer.DataWriter

/** Implement this trait to use ParseNN with arbitrary data sources
  *
  * Data sources such as Git, SVN, simple files or database sources can be
  * integrated by implementing this trait.
  *
  * @param parsers a reference to the parsers for filtering and parsing
  * @see GitAgent for an example implementation.
  */
abstract class SourceAgent(val parsers: List[Parser]) {
  /** prepare the files and revisions (e.g. clone and traverse a Git repo) */
  def load()
  /** parse the code, writing results using the given DataWriter */
  def parse(writer: DataWriter)
  /** read the contents of a specific revision of a file
    * @see FileRevision */
  def cleanup()
  /** get a map of revision numbers to revision metadata and files
    * @see Revision, @see FileRevision */
  def readFileRevision(f: FileRevision): Option[CharBuffer]
  /** delete temporary files (such as the cloned repository etc.) */
  def getRevisionFiles(): Option[SortedMap[Int,(Revision, Set[_ <: FileRevision])]]
  /** already implemented: get a map of revision numbers to revision metadata */
  final def getRevisions(): Option[SortedMap[Int, Revision]] = {
    Some(getRevisionFiles.get.map { case (i, r) => i -> r._1 })
  }
}

import java.util.Date
import java.util.TimeZone

// a revision consists of sequential index, a rev string (sha) and references to
// the previous and next revisions before and after this one
case class Revision(n: Int, rev: String,
                    authorDate: Date, authorTz: TimeZone, authorName: String, authorEmail: String,
                    committerDate: Date, committerTz: TimeZone, committerName: String, committerEmail: String,
                    var prev: Option[Revision], var next: Option[Revision],
                    var misc: Map[String,Any] = Map())
                    extends Ordered[Revision] {
  override def toString: String = s"$n ($rev)"
  override def hashCode = n.hashCode
  override def equals(that: Any) = {
    that match {
      case r: Revision => r.n == n
      case _ => false
    }
  }
  private def formatDate(date: Date, tz: TimeZone): String = {
    val df = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mmXXX")
    df.setTimeZone(tz)
    df.format(date)
  }
  def getAuthorDateString(): String = { formatDate(authorDate, authorTz) }
  def getCommitterDateString(): String = { formatDate(committerDate, committerTz) }
  def compare(that: Revision) = this.n - that.n
}

case class RevisionRange(start: Revision, end: Revision) extends Ordered[RevisionRange] {
  def compare(that: RevisionRange) = {
    if (this.start.n != that.start.n) { this.start.n - that.start.n }
    else { this.end.n - that.end.n }
  }
  override def equals(that: Any) = {
    that match {
      case r: RevisionRange => r.start == start && r.end == end
      case _ => false
    }
  }
  override def toString: String = s"[${start.n}-${end.n}]"
  def toCommitsString: String = s"[${start.rev}-${end.rev}]"
}

/** Change types concerning a file in a specific revision. For example, a file
  * could have been newly created, deleted, renamed or modified. The special
  * "Preexisting" type is used when the first selected revision is not equal to
  * the very initial revision stored in the repository. In that case, all files
  * that exist in the first selected revision, but were not actually created or
  * otherwise modified in that revision, are considered pre-existing.
  * @see FileRevision */
sealed abstract class ChangeType
// for files created before this revision
case object Preexisting extends ChangeType
// for files that did not exist in the previous revision
case object Created extends ChangeType
// for files that existed in the previous revision, but not anymore
case object Deleted extends ChangeType
// for files that were modified
case object Modified extends ChangeType
// for files that were renamed
case object Renamed extends ChangeType

/** Classes implementing FileRevision hold information on a specific revision
  * (uniquely identified, e.g. by a hash) of a specific file (uniquely
  * identified in the context of the source, e.g. by the path) and the change
  * type that the file underwent in that specific revision (@see ChangeType).
  * They may hold additional, source-specific information that the corresponding
  * agent requires to read the file revision. @see GitFileRevision for an
  * an example implementation.
  */
trait FileRevision {
  def fileId: String
  def revId: String
  def changeType: ChangeType
  override def toString(): String = s"[$fileId] $revId: $changeType"
}

