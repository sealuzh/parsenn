package ch.uzh.ifi.seal.parsenn.misc

import java.nio.file._
import java.nio.file.attribute._
import java.io.IOException

object Profiling {
  def time[R](block: => R): (R, Long) = {
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()
    (result, (t1 - t0))
  }
}

object FileTools {
  def removeRecursively(path: Path) {
    Files.walkFileTree(path, new RemovalFileVisitor)
  }
  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }
}

// Adapted for scala from
// http://stackoverflow.com/questions/779519/delete-files-recursively-in-java/8685959#8685959
class RemovalFileVisitor extends SimpleFileVisitor[java.nio.file.Path] {
  override def visitFile(file: Path,
                         attrs: BasicFileAttributes): FileVisitResult = {
    Files.delete(file)
    return FileVisitResult.CONTINUE
  }

  override def visitFileFailed(file: Path,
                               exc: IOException): FileVisitResult = {
    // try to delete the file anyway, even if its attributes
    // could not be read, since delete-only access is
    // theoretically possible
    Files.delete(file)
    return FileVisitResult.CONTINUE
  }

  override def postVisitDirectory(dir: Path,
                                  exc: IOException): FileVisitResult = {
    if (exc == null) {
      Files.delete(dir)
      return FileVisitResult.CONTINUE
    }
    else {
      // directory iteration failed propagate exception
      throw exc
    }
  }
}

