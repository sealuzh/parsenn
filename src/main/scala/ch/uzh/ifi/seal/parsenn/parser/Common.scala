package ch.uzh.ifi.seal.parsenn.parser

import org.antlr.v4.runtime.tree.Tree
import java.nio.CharBuffer
import ch.uzh.ifi.seal.parsenn.writer.DataWriter

trait Domain {
  val mapping = Map[Symbol,Set[String]]()
  lazy val labels = mapping.values.toSeq.flatten.toSet
  def is(domain: Domain) = this == domain
  def lookup(typeLabel: String, candidates: Symbol*): Boolean = {
    candidates.exists { c =>
      mapping.get(c) match {
        case Some(labels) => labels.contains(typeLabel)
        case _ => c match {
          case 'file if typeLabel == "META-FILE" => true
          case _ => false
        }
      }
    }
  }
}

object AllDomain extends Domain {
  override def lookup(typeLabel: String, candidates: Symbol*): Boolean = true
}

/** Parser interface
  *
  * A "parser" can be anything that loads data from a CharBuffer into the graph.
  */
abstract class Parser {

  /** A list of suffixes indicating which file types the parser supports */
  val suffixes: List[String]

  def parse(files: List[(String,CharBuffer)], writer: DataWriter) { }

}

case class NodeData(text: String, lineNumber: Int, parent: String, depth: Int, context: Tree)

