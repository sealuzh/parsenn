package ch.uzh.ifi.seal.parsenn.parser

import org.antlr.v4.runtime.ANTLRInputStream
import org.antlr.v4.runtime.CommonTokenStream

import ch.uzh.ifi.seal.parsenn.antlr.JavaParser
import ch.uzh.ifi.seal.parsenn.antlr.JavaLexer

object AntlrJavaParseTree extends Domain {
  override val mapping = Map(
    'class     -> Set("ClassDeclaration"),
    'block     -> Set("Block",
                      "ParExpression"),
    'statement -> Set("BlockStatement"),
    'ifStat    -> Set("IfStatement"),
    'whileStat -> Set("WhileStatement",
                      "DoStatement"),
    'forStat   -> Set("ForStatement"),
    'fork      -> Set("IfStatement",
                      "WhileStatement",
                      "ForStatement",
                      "CatchClause",
                      "SwitchLabel",
                      "ConditionalExpression",
                      "TryStatement",
                      "DoStatement"),
    'method    -> Set("MethodDeclaration",
                      "GenericMethodDeclaration",
                      "ConstructorDeclaration",
                      "InterfaceMethodDeclaration",
                      "GenericInterfaceMethodDeclaration"),
    'variable  -> Set("LocalVariableDeclaration"),
    'field     -> Set("FieldDeclaration"),
    'interface -> Set("InterfaceDeclaration"),
    'parameter -> Set("FormalParameter"),
    'variableId -> Set("VariableDeclaratorId")
  )
}

object AntlrJavaParser extends AntlrParser[JavaParser](AntlrJavaParseTree, filter = false) {
  override val suffixes = List(".java")
  override def lex(input: ANTLRInputStream) = new JavaLexer(input)
  override def parse(tokens: CommonTokenStream) = new JavaParser(tokens)
  override def enter(parser: JavaParser) = parser.compilationUnit()
}


