package ch.uzh.ifi.seal.parsenn.writer

import scala.io.Source
import scala.collection.mutable.{Map => MMap}
import java.io.FileWriter
import java.nio.file.Path
import java.nio.file.Paths

class Vocab(outDir: Path, fileName: String) {
  val vocab = MMap[String,Int]()
  val wordCount = MMap[String,Int]()
  // vocabs and writers used continuously during parsing
  val startVocab = Map(
    "_PAD" -> 0,
    "_GO" -> 1,
    "_EOS" -> 2,
    "_UNK" -> 3
  )

  def embed(s: String): Seq[Int] = {
    s.map { c => addWord(c.toString) }
  }

  def addWord(s: String): Int = {
    wordCount += (s -> (wordCount.getOrElseUpdate(s, 0) + 1))
    vocab.getOrElseUpdate(s, vocab.size)
  }

  private def sorted(): List[String] = {
    startVocab.toList.map(_._1) ++ (
      wordCount.toList.sortBy(_._2).foldLeft(List[String]()) { (l, k) =>
        k._1 :: l
      }
    )
  }

  private def writeVocab(vocab: List[String], outFile: Path) {
    val writer = new FileWriter(outDir.resolve(outFile).toFile)
    vocab.foreach { v => writer.write(v); writer.write('\n') }
    writer.close
  }

  private def mapVocab(vocabOld: MMap[String, Int], vocabNew: Map[String,Int]): Map[Int,Int] = {
    vocabOld.foldLeft(Map[Int,Int]()) { (m, v) =>
      m + (v._2 -> vocabNew.get(v._1).get)
    }
  }

  private def translateInts(intMap: Map[Int,Int], inFilePath: Path, outFilePath: Path): FileWriter = {
    val writer = new FileWriter(outFilePath.toFile)
    Source.fromFile(inFilePath.toFile).getLines.foreach { line =>
      if (line.nonEmpty) {
        val ints = line.split(" ").map(_.toInt)
        val translatedInts = ints.map { n => intMap.get(n).get }
        writer.write(translatedInts.mkString(" "))
      }
      writer.write('\n')
    }
    writer.close
    writer
  }

  def postprocess(): FileWriter = {
    val vocabSorted = sorted
    writeVocab(vocabSorted, Paths.get(fileName + ".vocab"))
    val vocabMap = mapVocab(vocab, vocabSorted.zipWithIndex.toMap)
    translateInts(vocabMap, outDir.resolve(Paths.get(fileName + ".ints.unsorted")), outDir.resolve(Paths.get(fileName + ".ints")))
  }

}

