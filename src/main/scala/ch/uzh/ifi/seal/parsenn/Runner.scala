package ch.uzh.ifi.seal.parsenn

import scala.io.Source
import java.nio.file.Paths
import com.typesafe.scalalogging.LazyLogging

import ch.uzh.ifi.seal.parsenn.parser._
import ch.uzh.ifi.seal.parsenn.writer._
import ch.uzh.ifi.seal.parsenn.source.GitAgent

object ParseNN extends App with LazyLogging {
  val allLanguages = List("java", "go")
  val allFormats = "all" :: TokenFormatter.all.map(_.label) ++ AstNodeFormatter.all.map(_.label)
  val usage = s"""Usage: sbt "run-main ch.uzh.ifi.seal.parsenn.ParseNN <urls-file> <languages> <formats> <out-dir> [<tmp-dir>]"
<urls-file> a text file containing any number of git urls, exactly one per line
<languages> any of ${allLanguages.mkString(",")}
<formats> any of ${allFormats.mkString(",")}
<out-dir> the path where output files should be written
<tmp-dir> the path where the git repositories will be stored temporarily (optional)
Example: sbt "run-main ch.uzh.ifi.seal.parsenn.ParseNN repos/demo-projects.txt java all /tmp/results
"""
  val arglist = args.toList
  type OptionMap = Map[Symbol, Any]

  arglist match {
    case projects :: lang :: method :: outDir :: rest =>
      require(allLanguages.contains(lang), "invalid language")
      val methods = method.split(",").map { m =>
        require(allFormats.contains(m), s"invalid method ${m}")
        m
      }
      rest match {
        case tempDir :: Nil => run(projects, lang, methods, outDir, tempDir)
        case _ =>              run(projects, lang, methods, outDir)
      }
    case _ => println(usage)
  }

  def run(projects: String, lang: String, methods: Array[String], outDir: String, tempDir: String = "/tmp/parsenn-gitrepos/") {
    val gitUrls = Source.fromFile(Paths.get(projects).toFile).getLines
    val tokenFormatters = if (methods.contains("all")) { TokenFormatter.all
    } else { TokenFormatter.all.filter{ t => methods.contains(t.label) } }
    val astNodeFormatters = if (methods.contains("all")) { AstNodeFormatter.all
    } else { AstNodeFormatter.all.filter{ t => methods.contains(t.label) } }
    val writer = new DataWriter(Paths.get(outDir), tokenFormatters, astNodeFormatters)
    val parsers = lang match {
      case "java" => List[Parser](AntlrJavaParser)
      case "go" => List[Parser](AntlrGoParser)
      case _ => List[Parser](AntlrJavaParser, AntlrGoParser)
    }
    gitUrls.zipWithIndex.foreach { case (url, i) =>
      logger.info(s"Parsing project #${i}: ${url}")
      val sources = new GitAgent(parsers, url, tempDir)
      sources.load
      sources.parse(writer)
      sources.cleanup
    }
    writer.close()
  }
}

