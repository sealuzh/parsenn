package ch.uzh.ifi.seal.parsenn.writer

import com.typesafe.scalalogging.Logger

trait TokenFormatter {
  def label: String = "tok/"
  def logger = Logger(label)
  def mkWords(line: String, tokens: List[String]): Seq[String]
  def mkInts(line: String, tokens: List[String], words: Seq[String], vocab: Vocab): Seq[Int] = {
    words.map { c => vocab.addWord(c) }
  }
}
object TokenFormatter {
  def all = List(Endings01, TakeDropWhile)
  def none = List()
}

/** binary representation whether or not a character ends a token.
 *  For example, here is a line with the corresponding binary mask:
 *  public static void   main(String[] args)
 *  000001 000001 0001   0001100000111 00011
 */
object Endings01 extends TokenFormatter {
  override def label = super.label + "endings01"
  def tokensToBinary(line: String, tokens: List[String]): String = {
    val result = new StringBuilder()
    var tail = line
    for (t <- tokens) {
      val tokenLength = t.size
      // all except the last character of a token are 0
      val zeroes = "0" * (tokenLength - 1)
      result ++= zeroes
      // the last character is a 1 indicating the end of the token
      result += '1'
      // drop the token from the input line
      tail = tail.drop(tokenLength)
      // translate any token-trailing spaces 1:1 to the output sequence
      val spaces = tail.takeWhile(_.toString.matches("""\s"""))
      tail = tail.drop(spaces.size)
      result ++= spaces
    }
    result.toString
  }
  def mkWords(line: String, lineTokens: List[String]): Seq[String] = {
    val binaryLine = tokensToBinary(line, lineTokens.map(_.toString))
    if (binaryLine.size == binaryLine.size) {
      return binaryLine.map(_.toString)
    }
    else {
      logger.warn(s"tokensToBinary size mismatch:\n${line}\n${binaryLine}")
      return List[String]()
    }
  }
}

/** Formatter that represents take/drop operations on the input sequence
 *  Assumes that resulting tokens are always trimmed. Each operation is
 *  formatted as (T|D):C:N where T/D is the operation (take or drop), C is the
 *  last character (the condition) and N is the number of times the condition
 *  should be met (usually 1 except if the last character also appears in the
 *  token)
 */
object TakeDropWhile extends TokenFormatter {
  import scala.collection.mutable.ListBuffer
  override def label = super.label + "takeDropWhile"
  def interpreter(line: String, instructionString: String): List[String] = {
    val tokens = new ListBuffer[String]()
    val instructions = instructionString.split(" ")
    if (instructions.size == 1 && instructions.head.size == 0) {
      return List[String]()
    }
    var tail = line
    instructions.foreach { i =>
      var instruction = i
      val kind = instruction.takeWhile(_ != ':')
      instruction = instruction.drop(kind.size + 1)
      var count = instruction.takeWhile(_ != ':').toInt
      instruction = instruction.drop(kind.size + count.toString.size)
      val char = instruction.head
      val token = new StringBuilder()
      kind match {
        case "D" => while (count > 0) {
          tail = tail.dropWhile(_ != char)
          tail = tail.drop(1)
          count -= 1;
        }
        case "T" => 
          while (count > 0) {
            val t = tail.takeWhile(_ != char)
            token ++= t
            tail = tail.drop(t.size)
            token ++= tail.take(1)
            tail = tail.drop(1)
            count -= 1;
          }
          tokens += token.toString.trim
        case _ => logger.error(s"invalid instruction ${i}")
      }
    }
    tokens.toList
  }
  def mkWords(line: String, tokens: List[String]): Seq[String] =  {
    val result = new ListBuffer[String]()
    var tail = line
    // For each token, determine the drop/take operations necessary
    for (t <- tokens) {
      // determine start/end of token
      val tokenStartIndex = tail.indexOfSlice(t)
      if (tokenStartIndex < 0) {
        logger.warn(s"Couldn't find token ${t} in input ${tail}")
        return List[String]()
      }
      val leading = tail.substring(0, tokenStartIndex).trim
      // Must drop some characters before the token
      if (leading.size > 0) {
        val last = leading.last
        result += "D:" + (leading.count(_ == last)) + ":" + last
      }
      tail = tail.drop(tokenStartIndex)
      val last = t.last
      result += "T:" + (t.count(_ == last)) + ":" + last
      tail = tail.drop(t.size)
    }
    val instructions = result
    val appliedResult = interpreter(line, instructions.mkString(" "))
    logger.debug(s"S: ${instructions}")
    logger.debug(s"I: ${tokens.mkString("-")}")
    logger.debug(s"O: ${appliedResult.mkString("-")}")
    if (!tokens.sameElements(appliedResult)) {
      logger.warn(s"instructions\n${instructions}\non input\n${line}\nproduce wrong tokens\n${appliedResult.mkString("|")}\ninstead of correct tokens\n${tokens.mkString("|")}}")
      List[String]()
    } else {
      instructions
    }
  }
}

/** Output as a series of instructions on the original input.
 *  For example, here is a line with the corresponding instructions:
 *  public String s = "Hello, World";
 *  0:S 0:S 0:S 0:S 2-" 1-;
 */
object Instructions extends TokenFormatter {
  import scala.collection.mutable.ListBuffer
  override def label = super.label + "instructions"
  def interpreter(line: String, instructionString: String): List[String] = {
    val tokens = new ListBuffer[String]()
    val instructions = instructionString.split(" ")
    if (instructions.size == 1 && instructions.head.size == 0) {
      return List[String]()
    }
    var tail = line
    instructions.foreach { i =>
      var count = i.takeWhile(_.toString.matches("""[0-9]""")).toInt
      val kind = i.dropWhile(_.toString.matches("""[0-9]""")).take(1)
      val char = i.dropWhile(_.toString.matches("""[0-9]""")).drop(1).head
      val token = new StringBuilder()
      kind match {
        case ":" => while (count > 0) {
          val t = tail.takeWhile(!_.toString.matches("""\s"""))
          token ++= t
          tail = tail.drop(t.size)
          token ++= tail.take(1)
          tail = tail.drop(1)
          count -= 1;
        }
        case "-" => while (count > 0) {
          val t = tail.takeWhile(_ != char)
          token ++= t
          tail = tail.drop(t.size)
          token ++= tail.take(1)
          tail = tail.drop(1)
          count -= 1;
        }
        case _ => logger.error(s"invalid instruction ${i}")
      }
      tokens += token.toString.trim
    }
    tokens.toList
  }
  def mkWords(line: String, tokens: List[String]): Seq[String] =  {
    val result = new ListBuffer[String]()
    var tail = line
    for (t <- tokens) {
      val length = t.size
      val input = tail.take(length)
      val followChar = tail.drop(length).take(1)
      val followedBySpace = followChar.matches(""" """)
      // Easiest way: Take everything until the next space (if no spaces in token)
      if (followedBySpace) {
        result += (input.count(_.toString.matches("""\s""")) + 1) + ":S"
        tail = tail.drop(length)
        val spaces = tail.takeWhile(_.toString.matches("""\s"""))
        tail = tail.drop(spaces.size)
      }
      // If it's not followed by a space, use the last character as an indicator
      if (!followedBySpace) {
        val lastChar = t.last
        result += s"${input.count(_ == lastChar)}-${lastChar}"
        tail = tail.drop(length)
        val spaces = tail.takeWhile(_.toString.matches("""\s"""))
        tail = tail.drop(spaces.size)
      }
    }
    val instructions = result
    val appliedResult = interpreter(line, instructions.mkString(" "))
    logger.debug(s"S: ${instructions}")
    logger.debug(s"I: ${tokens.mkString("-")}")
    logger.debug(s"O: ${appliedResult.mkString("-")}")
    if (!tokens.sameElements(appliedResult)) {
      logger.warn(s"instructions\n${instructions}\non input\n${line}\nproduce wrong tokens\n${appliedResult.mkString("|")}\ninstead of correct tokens\n${tokens.mkString("|")}}")
      List[String]()
    } else {
      instructions
    }
  }
}


