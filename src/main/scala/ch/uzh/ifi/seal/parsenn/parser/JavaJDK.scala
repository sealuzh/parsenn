package ch.uzh.ifi.seal.parsenn.parser

import scala.collection.JavaConverters._
import scala.language.existentials
import javax.tools.JavaFileObject
import com.sun.source.util.SimpleTreeVisitor
import java.util.{List => JavaList}
import java.util.{Set => JavaSet}
import com.sun.source.tree._
import com.sun.source.util.SimpleTreeVisitor
import com.sun.tools.javac.api.JavacTool
import java.lang.reflect.ParameterizedType
import sun.reflect.generics.reflectiveObjects.WildcardTypeImpl
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.nio.CharBuffer

import java.net.URI
import javax.tools.JavaFileObject.Kind
import com.sun.source.tree.Tree.{Kind => TreeKind}
import javax.tools.SimpleJavaFileObject
import java.lang.reflect.Method
import java.lang.Class
import java.lang.reflect.{Type => ReturnType}

class NullOutputStream extends OutputStream {
  override def write(b: Int) = { }
  override def write(b: Array[Byte]) = { }
}

class JavaSourceFromCharArray(name: String, val code: CharBuffer)
    extends SimpleJavaFileObject(URI.create("string:///" + name), Kind.SOURCE) {

  override def getCharContent(ignoreEncodingErrors: Boolean): CharSequence = {
    return code;
  }
}

object JDKJavaParser {
  // Calls to java.lang.reflect.Method.getGenericReturnType are very expensive,
  // so we cache the class methods and method return types.
  private val methodCache: Map[Class[_ <: Tree], Array[Method]] = TreeKind.values
              .filter(_ != TreeKind.OTHER)
              .foldLeft(Map[Class[_ <: Tree], Array[Method]]()) {
    case (acc, n) => acc + (n.asInterface -> n.asInterface.getDeclaredMethods)
  }
  private val returnTypesCache: Map[Method, ReturnType] = methodCache.values.flatten
              .foldLeft(Map[Method, ReturnType]()) {
    case (acc, n) => acc + (n -> n.getGenericReturnType)
  }

  def parse(files: List[(String,CharBuffer)]) {
    val compiler = JavacTool.create
    val fileObjects: List[JavaFileObject] = files.map { case (path, code) =>
      new JavaSourceFromCharArray(path, code)
    }
    val javac = compiler.getTask(new OutputStreamWriter(new NullOutputStream()),
                                 null, null, null, null, fileObjects.asJava)
    try {
      val trees = javac.parse
      trees.asScala.foreach { tree =>
        val fileName = tree.getSourceFile.getName
        val visitor = new UniversalTreeVisitor(fileName, methodCache, returnTypesCache)
        val context = ParsingContext("CompilationUnit", fileName, 0)
        tree.accept(visitor, context)
      }
    }
    catch { case e: Exception =>
      println(s"ERROR ${e}")
    }

    Console.flush()
  }
}



// vertices being visited use a ParsingContext to retrieve context information and
// pass information to their children
case class ParsingContext(
  relation: String,
  parentUri: String,
  index: Int = 0
)

class UniversalTreeVisitor(fileName: String, methodCache: Map[Class[_ <: Tree],Array[Method]], returnTypesCache: Map[Method,ReturnType])
  extends SimpleTreeVisitor[Void, ParsingContext] {

  override def defaultAction(tree: Tree, c: ParsingContext): Void = {
    // retrieve the interface of the specialized Tree object
    val treeInterface = tree.getKind.asInterface
    val treeTypeName = tree.getKind.toString
    val relation = c.relation
    var parentUri = c.parentUri
    var typeString = treeTypeName

    relation match {
      // parameter VARIABLEs shall be known as PARAMETERs
      case "Parameters" if typeString == "VARIABLE" =>
        typeString = "PARAMETER"
      case "Statements" =>
        val uri = parentUri + "/Statement" + c.index
      case _ =>
    }

    val uri = parentUri + "/" + relation + c.index
    val relevantParentUri = uri

    val treeKind = tree.getKind
    var gotChildren = false

    var literals = Map[String, String]()

    // for each method declared by the Tree vertex, check the return type of the
    // method. If it's a Tree or collection of Trees, descend. Else, call the
    // method and store the return value.
    var treeKindChildCounter = 0
    methodCache.get(treeInterface).get.foreach{ m =>
      returnTypesCache.get(m).get match {

        // if the return type is parameterized (e.g. Foo<? extends T>), then
        // check if it's a List<? extends Tree>
        case t: ParameterizedType => {

          val rawType = t.getRawType
          val typeArguments = t.getActualTypeArguments

          // check if it is a List
          val (isList, isSet) = rawType match {
            case r: Class[_] =>
              if (classOf[JavaList[_]].isAssignableFrom(r)) { (true, false) }
              else if (classOf[JavaSet[_]].isAssignableFrom(r)) { (false, true) }
              else { (false, false) }
            case _ => (false, false)
          }

          // if both are true, traverse the children
          if (isList) {
            // check if it is parameterized as <? extends Tree>
            val containsTrees = typeArguments.headOption match {
              case Some(wildcard: WildcardTypeImpl) =>
                wildcard.getUpperBounds.headOption match {
                  case Some(bound) =>
                    if (bound.isInstanceOf[Class[_ <: Tree]]) { true }
                    else { false }
                  case _ => false
                }
              case _ => false
            }
            if (containsTrees) {

              val children = Option(m.invoke(tree).asInstanceOf[JavaList[_ <: Tree]])
              children match {
                case Some(children) =>
                  if (children.size > 0) { gotChildren = true; print(s"${treeKind}( ") }
                  children.asScala.zipWithIndex.foreach{ case(child,i) =>
                    val context = c.copy(parentUri = relevantParentUri,
                                         relation = m.getName.drop(3),
                                         index = i)
                    super.visit(child, context)
                  }
                  if (children.size > 0) { print(s")${treeKind} ") }
                case None =>
              }
            }
            else {
              println(s"WARNING: got a List with non-Tree contents from Tree" +
                      s"'$treeInterface', method '$m': $t")
            }
          }
          else if (isSet) {
            val items = m.invoke(tree).asInstanceOf[JavaSet[_]]
            items.asScala.zipWithIndex.foreach{ case(child,i) =>
              print(s"${i.toString} ")
              literals = literals + (s"${m.getName.drop(3)}:$child" -> i.toString)
            }
          }
          else {
            println(s"WARNING: unknown collection from Tree '$treeInterface' " +
                    s"method '$m': $t")
            // TODO: what else? log a warning?
          }
        }

        // if the return type is a Tree, simply visit it
        case t if classOf[Tree].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val childVertex = m.invoke(tree).asInstanceOf[Tree]
          val context = c.copy(parentUri = relevantParentUri,
                               relation = m.getName.drop(3),
                               index = treeKindChildCounter)
          treeKindChildCounter += 1
          super.visit(childVertex, context)
        }

        // if the return type is a LineMap, do nothing
        case t if classOf[LineMap].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
        }

        // if the return type is an Element, store the method name (minus the
        // "get" prefix) and the return value as a string literal
        case t if classOf[javax.lang.model.element.Name].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val name = Option(m.invoke(tree).asInstanceOf[javax.lang.model.element.Name])
          name.foreach { value =>
            literals = literals + (s"${m.getName.drop(3)}" -> value.toString)
          }
        }

        // if the return type is a TypeKind, store the method name (minus the
        // "get" prefix) and the return value as a string literal
        case t if classOf[javax.lang.model.`type`.TypeKind].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val typeKind = m.invoke(tree).asInstanceOf[javax.lang.model.`type`.TypeKind]
          literals = literals + (s"${m.getName.drop(3)}" -> typeKind.toString)
        }

        // if the return type is a FileObject, store the method name (minus the
        // "get" prefix) and the return value as a string literal
        case t if classOf[javax.tools.JavaFileObject].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val file = m.invoke(tree).asInstanceOf[javax.tools.JavaFileObject]
          literals = literals + (s"${m.getName.drop(3)}" -> file.getName)
        }

        // if the return type is a Boolean, store the method name and the return
        // value as a boolean literal
        case t if classOf[Boolean].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val bool = m.invoke(tree).asInstanceOf[Boolean]
          literals = literals + (s"${m.getName}" -> m.getName)
        }

        // if the return type is an Object, store the method name (minus the
        // "get" prefix) and the return value as a literal of no specific type
        case t if classOf[java.lang.Object].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val obj = m.invoke(tree).asInstanceOf[java.lang.Object]
          literals = literals + (s"${m.getName.drop(3)}" ->
                                 (if (obj == null) "null" else obj.toString))
        }

        // if the return type is anything else (which shouldn't typically be),
        // then just log a warning
        case t => {
          // TODO: log warning about unknown return type if reaching this
          println(s"WARNING: unknown return type from Tree '$treeInterface' " +
                  s"method '$m': $t")
        }
      }
    }

    if (!gotChildren) { print(s"${treeKind} ") }

    if (uri == relevantParentUri) {
      // SimpleName needs to be renamed for compatibility with NameAnalysis
      literals = literals.get("SimpleName") match {
        case Some(name) => literals - "SimpleName" + ("Name" -> name)
        case _ => literals
      }
    }

    super.defaultAction(tree, c)
  }

}

