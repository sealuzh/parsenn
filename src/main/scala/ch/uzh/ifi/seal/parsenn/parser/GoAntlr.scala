package ch.uzh.ifi.seal.parsenn.parser

import org.antlr.v4.runtime.ANTLRInputStream
import org.antlr.v4.runtime.CommonTokenStream

import ch.uzh.ifi.seal.parsenn.antlr.GolangParser
import ch.uzh.ifi.seal.parsenn.antlr.GolangLexer

object AntlrGoParser extends AntlrParser[GolangParser](AllDomain, filter = false) {
  override val suffixes = List(".go")
  override def lex(input: ANTLRInputStream) = new GolangLexer(input)
  override def parse(tokens: CommonTokenStream) = new GolangParser(tokens)
  override def enter(parser: GolangParser) = parser.sourceFile()
}


