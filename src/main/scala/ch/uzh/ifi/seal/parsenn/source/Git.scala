package ch.uzh.ifi.seal.parsenn.source

import scala.collection.immutable.SortedMap
import scala.collection.mutable.ListBuffer
import scala.language.existentials
import java.nio.ByteBuffer
import java.nio.CharBuffer
import java.nio.charset.StandardCharsets
import java.nio.file._
import com.typesafe.scalalogging.LazyLogging
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.Constants
import org.eclipse.jgit.lib.FileMode
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.revwalk.RevSort
import org.eclipse.jgit.revwalk.RevWalk
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.eclipse.jgit.treewalk.TreeWalk
import org.eclipse.jgit.treewalk.filter.OrTreeFilter
import org.eclipse.jgit.treewalk.filter.PathSuffixFilter
import org.eclipse.jgit.treewalk.filter.TreeFilter

import ch.uzh.ifi.seal.parsenn.parser.Parser
import ch.uzh.ifi.seal.parsenn.writer.DataWriter
import ch.uzh.ifi.seal.parsenn.misc.FileTools

/** Extends FileRevision by also storing the Git ObjectId for each blob */
case class GitFileRevision (fileId: String, revId: String,
                            changeType: ChangeType, objectId: ObjectId)
                            extends FileRevision

/** SourceAgent implementation for Git repositories; supports async parsing
  *
  * @param parsers a list of selected parsers
  * @param url the Git url
  * @param start start revision hash as a string
  * @param end end revision hash as a string
  * @see SourceAgent
  */
class GitAgent(parsers: List[Parser], url: String, localDirPath: String,
               start: Option[String] = None, end: Option[String] = None)
               extends SourceAgent(parsers) with LazyLogging  {

  // loading the revisions may take some time
  var revisions: Option[SortedMap[Int, (Revision, Set[GitFileRevision])]] = None
  val localDir = Paths.get(localDirPath)
  val gitDir = localDir.resolve(".git")
  val builder = new FileRepositoryBuilder
  val repository = builder.setGitDir(gitDir.toFile)
    .readEnvironment
    .findGitDir
    .build

  /** clones the repository. Directory at localDirPath will be overwritten! */
  override def load() = {
    if (Files.exists(localDir)) {
      FileTools.removeRecursively(localDir)
    }
    logger.info(s"cloning repository ${url}...")
    if (Files.exists(localDir)) {
    }
    val timestamp = System.nanoTime()
    Git.cloneRepository
        .setURI(url)
        .setDirectory(localDir.toFile)
        .call
    val duration = (System.nanoTime - timestamp)/1000000000.0
    logger.info(f"clone duration: ${duration}%3.2fs")
    logger.debug("cloning finished")
    loadRevisions()
  }

  /** parse the source given a writer */
  override def parse(writer: DataWriter) = {
    logger.info("parsing source code")
    if (!Files.exists(localDir)) {
      logger.error("ERROR: checkoutInitial - code dir does not exist")
    }
    else {
      var numberOfFiles = 0
      getRevisionFiles.get.foreach { case (i, revisionData) =>
        val (rev, files) = (revisionData._1, revisionData._2)
        logger.debug(s"Queuing files in revision ${rev.n} (${rev.rev.substring(0,7)}) for parsing")
        files.filter { f =>
          // if an appropriate parser exists
          parsers.exists(_.suffixes.exists(f.fileId.endsWith(_)))
        }.map { f =>
          numberOfFiles += 1
          // parse the file
          if (f.changeType != Deleted) {
            val fileChars = readFileRevision(f).get
            parsers.foreach { parser =>
              if (parser.suffixes.exists(f.fileId.endsWith(_))) {
                parser.parse(List((f.fileId, fileChars)), writer)
              }
            }
          }
        }
      }
      logger.info(s"done parsing ${numberOfFiles} files")
    }
  }

  /** Removes the local clone directory */
  override def cleanup() = {
    if (Files.exists(localDir)) {
      FileTools.removeRecursively(localDir)
    }
  }

  override def getRevisionFiles(): Option[SortedMap[Int,(Revision, Set[_ <: FileRevision])]] = {
    revisions
  }

  override def readFileRevision(f: FileRevision): Option[CharBuffer] = {
    f match {
      case gf: GitFileRevision =>
        val fileLoader = repository.open(gf.objectId)
        val fileBytes = ByteBuffer.wrap(fileLoader.getCachedBytes)
        val fileChars = StandardCharsets.UTF_8.decode(fileBytes)
        Some(fileChars)
      case _ => None
    }
  }

  def loadRevisions() = {
    logger.info("reading Git metadata")
    val timestamp = System.nanoTime()

    // open the bare git repository
    val git = new Git(repository)

    // Create a RevWalk which includes the part of the tree reaching from
    // the most recent commit to be parsed all the way back to the oldest
    // commit as specified by management
    val revWalk = new RevWalk(repository)

    // determine tree walk starting point (most recent, relevant commit)
    val endCommitId = end match {
      case Some(e) => repository.resolve(e)
      case _ => repository.resolve(Constants.HEAD)
    }
    val endCommit = revWalk.parseCommit(endCommitId);

    // this is called markStart because we're walking backwards in time, so
    // the end commit chosen by management is the starting point of the walk
    revWalk.markStart(endCommit)

    // determine tree walk ending point (oldest, relevant commit)
    val startCommitId = start match {
      case Some(s) => repository.resolve(s)
      case _ => repository.resolve(Constants.HEAD)
    }
    val startCommit = Some(revWalk.parseCommit(startCommitId))
    if (startCommit.get.getParentCount > 0) {
      val ignoreCommit = startCommit.get.getParent(0)
      revWalk.markUninteresting(ignoreCommit)
    }

    // start walking revisions back in time
    revWalk.sort(RevSort.REVERSE)
    val iterator = revWalk.iterator()
    revisions = Some(SortedMap())
    var previousRevision: Option[Revision] = None
    var revisionCount = 0
    while (iterator.hasNext()) {
      var revisionBuffer: ListBuffer[GitFileRevision] = new ListBuffer
      val reader = repository.newObjectReader

      // grab a commit
      val commit = iterator.next
      val commitId = commit.getId.getName
      logger.debug(s"Loading metadata for commit $revisionCount (${commitId.substring(0,7)})")

      // create a tree walk to traverse the file tree
      val walk = new TreeWalk(reader)
      walk.setRecursive(true)

      // get the file tree contained in the current commit
      val tree = commit.getTree
      walk.addTree(tree)

      // and also get the tree of the parent commit
      val gotParent = (commit.getParentCount > 0)
      if (gotParent) {
        val parentCommit = commit.getParent(0)
        val parentTree = parentCommit.getTree
        walk.addTree(parentTree)
      }

      val suffixes = parsers.map(_.suffixes).flatten
      val filter = suffixes.size match {
        case 1 => PathSuffixFilter.create(suffixes.head)
        case _ => OrTreeFilter.create(Array[TreeFilter](
          suffixes.map(PathSuffixFilter.create(_)):_*))
      }
      walk.setFilter(filter)

      // walk the commit tree
      while (walk.next()) {
        val objectId = walk.getObjectId(0)
        val fileMode = walk.getFileMode(0)
        if (gotParent) {
          val parentFileId = walk.getObjectId(1)
          val parentFileMode = walk.getFileMode(1)

          // if the two commits aren't refering identical files...
          if (objectId != parentFileId) {
            // and if there exists a parent commit, figure out if the file...
            val changeType = if (gotParent) {
              // is missing in this commit, because it got deleted...
              if (fileMode == FileMode.MISSING) { Deleted }
              // or if it is missing from the parent commit, because it got created.
              else if (parentFileMode == FileMode.MISSING) { Created }
              // otherwise it was modified
              else { Modified }
            }
            else {
              // if there's no parent commit, it must have been created
              Created
            }
            revisionBuffer += GitFileRevision(walk.getPathString.replace(' ','_'),
                                          commitId, changeType, objectId)
          }
          else {
            // if the oldest included commit is not the initial commit, we
            // also need to load all files which exist in that commit
            startCommit match {
              case Some(s) if s.equals(commit) =>
                val changeType = Preexisting
                revisionBuffer += GitFileRevision(walk.getPathString.replace(' ','_'),
                                              commitId, changeType, objectId)
              case _ =>
            }
          }
        }
        else {
          // otherwise it is the actual initial commit where all files are new
          val changeType = Created
          revisionBuffer += GitFileRevision(walk.getPathString.replace(' ','_'),
                                        commitId, changeType, objectId)
        }
        if (walk.isSubtree()) {
          walk.enterSubtree()
        }
      }

      // extract metadata
      val authorIdent = commit.getAuthorIdent
      val authorName = authorIdent.getName
      val authorEmail = authorIdent.getEmailAddress
      val authorDate = authorIdent.getWhen
      val authorTz = authorIdent.getTimeZone
      val committerIdent = commit.getCommitterIdent
      val committerName = committerIdent.getName
      val committerEmail = committerIdent.getEmailAddress
      val committerDate = committerIdent.getWhen
      val committerTz = committerIdent.getTimeZone

      // store metadata as a new Revision
      val revision = Revision(revisionCount, commitId,
                              authorDate, authorTz, authorName, authorEmail,
                              committerDate, committerTz, committerName, committerEmail,
                              previousRevision, None)

      // update previous revision to point to current new revision as next
      previousRevision match {
        case Some(r) => r.next = Some(revision)
        case None =>
      }
      previousRevision = Some(revision)
      revisions = Some(revisions.get + (revisionCount -> (revision, revisionBuffer.toSet)))
      revisionCount += 1
    }

    val duration = (System.nanoTime - timestamp)/1000000000.0
  }

}


