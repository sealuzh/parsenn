package ch.uzh.ifi.seal.parsenn.writer

import com.typesafe.scalalogging.Logger

import ch.uzh.ifi.seal.parsenn.parser.NodeData

trait AstNodeFormatter {
  def label: String = "ast/"
  def logger = Logger(label)
  def mkWords(line: String, nodes: List[NodeData]): Seq[String]
  def mkInts(line: String, nodes: List[NodeData], words: Seq[String], vocab: Vocab): Seq[Int] = {
    words.map { c => vocab.addWord(c) }
  }
}
object AstNodeFormatter {
  def all = List(NodeContextDepth, AnonIdentifiers)
  def none = List()
}

object NodeContextDepth extends AstNodeFormatter {
  override def label = super.label + "nodeContext-depth"

  def mkWords(line: String, nodes: List[NodeData]): Seq[String] = {
    nodes.map { n =>
      s"${n.parent}￨${n.depth}"
    }
  }

}

object AnonIdentifiers extends AstNodeFormatter {
  override def label = super.label + "anonIdentifiers"
  def mkWords(line: String, nodes: List[NodeData]): Seq[String] = {
    nodes.map { n =>
      if (n.context.getParent.getPayload.getClass.getSimpleName == "VariableDeclaratorIdContext") {
        "_VAR"
      } else {
        n.text
      }
    }
  }
}

