package ch.uzh.ifi.seal.parsenn.writer

object JavaCode {
  val demo1 = """
  public class Test {
    public static String s = "foo bar";
    public static void   main(String[] args)	{
      int n = 10;
      /* test */ String s = "Hello";
      System.out.println(s + ",	world" + " ! ",	 n);
    }
  }
  """
}

object GoCode {
  val demo1 = """
  package main

  import "fmt"

  func main() {
    fmt.Println("hello world")
  }
  """
}


