package ch.uzh.ifi.seal.parsenn.writer

import org.specs2.mutable.Specification

import scala.io.Source
import java.nio.CharBuffer
import java.nio.file.Paths

import ch.uzh.ifi.seal.parsenn.parser._

object WriterSpec extends Specification {

  val writer = new DataWriter(Paths.get("/tmp/parsenn"), TokenFormatter.all, AstNodeFormatter.all)
  AntlrJavaParser.parse(List(("demo1", CharBuffer.wrap(JavaCode.demo1))), writer)

  "ParseNN should tokenize the code and" >> {

    "store the original source (stripped whitespace)" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/tok/source").toFile).mkString
      res must startWith("public class Test {\n")
      res must haveSize(193)
    }

    "store individual characters" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/tok/source-chars").toFile).mkString
      res must startWith("p u b l i c ＀ c l a s s ＀ T e s t ＀ {\n")
      res must haveSize(370)
    }

    "store individual characters embedded" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/tok/source-chars.ints").toFile).mkString
      res must startWith("17 15 18 9 6 13 4 13 9 14 7 7 4 38 21 7 5 4 25\n")
      res must haveSize(470)
    }

    "store endings01 representation" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/tok/endings01").toFile).mkString
      res must startWith("0 0 0 0 0 1 ＀ 0 0 0 0 1 ＀ 0 0 0 1 ＀ 1\n")
      res must haveSize(346)
    }

    "store endings01 representation embedded" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/tok/endings01.ints").toFile).mkString
      res must startWith("4 4 4 4 4 5 6 4 4 4 4 5 6 4 4 4 5 6 5\n")
      res must haveSize(346)
    }

  }

  "ParseNN should parse the code into ASTs and" >> {

    "store the original source (stripped whitespace)" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/ast/source").toFile).mkString
      res must startWith("public class Test {\n")
      res must haveSize(193)
    }

    "store tokens" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/ast/tokens").toFile).mkString
      res must startWith("public class Test {\n")
      res must haveSize(196)
    }

    "store tokens (embedded)" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/ast/tokens.ints").toFile).mkString
      res must startWith("6 23 32 9\n")
      res must haveSize(129)
    }

    "store nodeContext|Depth" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/ast/nodeContext-depth").toFile).mkString
      res must startWith("ClassOrInterfaceModifier￨3")
      res must haveSize(880)
    }

    "store nodeContext|Depth (embedded)" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/ast/nodeContext-depth.ints").toFile).mkString
      res must startWith("24 9 9 13\n")
      res must haveSize(131)
    }

    "store anonIdentifiers" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/ast/anonIdentifiers").toFile).mkString
      res.split('\n')(1) must startWith("""public static String _VAR = "foo＀bar" ;""")
      res must haveSize(205)
    }

    "store anonIdentifiers (embedded)" in{
      val res = Source.fromFile(Paths.get("/tmp/parsenn/ast/anonIdentifiers.ints").toFile).mkString
      res.split('\n')(1) must startWith("7 11 6 4 8 22 5")
      res must haveSize(128)
    }


  }


  writer.close()

}

