name := "parsenn"

version := "0.0.1"

organization in ThisBuild := "ch.uzh.ifi.seal"

scalaVersion in ThisBuild := "2.12.0"

scalacOptions in ThisBuild ++= Seq(
  "-Ywarn-unused-import",
  "-feature",
  "-unchecked",
  "-deprecation",
  "-Xlint:-adapted-args"
)

unmanagedJars in Compile += file(System.getProperty("java.home").dropRight(3)+"lib/tools.jar")

antlr4Settings

antlr4PackageName in Antlr4 := Some("ch.uzh.ifi.seal.parsenn.antlr")

antlr4GenVisitor in Antlr4 := true

antlr4GenListener in Antlr4 := false

libraryDependencies ++= {
  Seq(
    "com.typesafe" % "config" % "1.3.1",
    "ch.qos.logback" % "logback-classic" % "1.1.7",
    "org.eclipse.jgit" % "org.eclipse.jgit" % "4.5.0.201609210915-r",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
    "com.github.tototoshi" %% "scala-csv" % "1.3.4",
    "org.antlr" % "antlr4" % "4.5.3",
    "org.specs2" %% "specs2-core" % "3.8.9" % "test"
  )
}

libraryDependencies in assembly ~= { _ map {
  case m if m.organization == "org.antlr" =>
    m.exclude("org.antlr", "antlr4")
  case m => m
}}

scalacOptions in Test ++= Seq("-Yrangepos")

pollInterval in ThisBuild := 1000

watchSources := watchSources.value.filter { ! _.getName.endsWith(".class") }

resolvers ++= Seq(
  "Maven Central Server" at "http://repo1.maven.org/maven2"
)

seq(Revolver.settings: _*)

